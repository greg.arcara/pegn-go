package json

// String returns an escaped version of the string suitable for
// inclusion as a JSON string value. Unlike Go's standard MarshalJSON,
// this function leaves Unicode as is without escaping it. It also does
// not dubiously escape HTML characters unnecessarily. In short, it
// doesn't overreach.
//
// The following PEGN describes the formal specification of a JSON
// string:
//
//     String  <-- DQ (Escaped / [x20-x21] / [x23-x5B]
//               / [x5D-x10FFFF])* DQ
//     Escaped  <- BKSLASH ("b" / "f" / "n" / "r"
//               / "t" / "u" hex{4} / DQ / BKSLASH / SLASH)
//
// Unicode and SLASH are not escaped here because they are considered
// valid JSON string data without the escape.
func String(s string) (escaped string) {
	for _, r := range s {
		switch r {
		case '\t':
			escaped += "\\t"
		case '\b':
			escaped += "\\b"
		case '\f':
			escaped += "\\f"
		case '\n':
			escaped += "\\n"
		case '\r':
			escaped += "\\r"
		case '\\':
			escaped += "\\\\"
		case '"':
			escaped += "\\\""
		default:
			escaped += string(r)
		}
	}
	return
}
