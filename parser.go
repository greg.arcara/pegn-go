// Copyright 2020 Rob Muhlestein.
// Use of this source code is governed by the Apache
// 2.0 license that can be found in the LICENSE file.

package pegn

import "fmt"

type Parser interface {
	fmt.Stringer
	Grammar() Grammar
	SetGrammar(g Grammar)
	Init(s interface{}) error
	Mark() *Mark
	Check(m ...interface{}) (*Mark, error)
	Expect(m ...interface{}) (*Mark, error)
	Expected(it interface{}) error
	Goto(m *Mark)
	Next()
	Move(n int)
	Done() bool
	Parse(m *Mark) string
	Slice(beg *Mark, end *Mark) string
	NewLine()
	Print()
}
