// Copyright 2020 Rob Muhlestein.
// Use of this source code is governed by the Apache
// 2.0 license that can be found in the LICENSE file.

package pegn

import "fmt"

// Position contains the user-land position for reporting back when
// there is a problem with parsing. This is different from internal
// position information used to parse from the internal data buffer.
// The Parser.NewLine() method should always increment the Pos.Line and
// set Pos.LineRune and Pos.LineByte.
type Pos struct {
	Line     int // lines (rows) starting at 1
	LineRune int // offset rune in line starting at 1
	LineByte int // offset byte in line starting at 1
	Rune     int // offset rune pos starting with 1
}

// Mark is a structure that points to specific position within buffered
// data, akin to a cursor. An internal Mark is used within the Parser.
// Marks are returned by methods of Parser implementations. Marks must
// be set to utf.ErrorRune and have Len set to 0 if the parser is asked
// to read beyond the end of the data.  Manipulating Mark values
// directly is discouraged.
type Mark struct {
	Pos
	Rune rune // last rune decoded
	Byte int  // beginning of last rune decoded
	Len  int  // length of last rune decoded (0-4)
	Next int  // beginning of next rune to decode
}

func (m *Mark) String() string {
	s := fmt.Sprintf(`%U %q %v,%v-%v (%v-%v)`,
		m.Rune, m.Rune,
		m.Pos.Line, m.Pos.LineRune, m.Pos.LineByte,
		m.Pos.Rune, m.Byte+1,
	)
	return s
}

// Print prints the mark itself.
func (m *Mark) Print() { fmt.Println(m) }
