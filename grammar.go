package pegn

type Grammar interface {
	Lang() string
	LangExt() string
	MajorVer() int
	MinorVer() int
	PatchVer() int
	PreVer() string
	Home() string
	Copyright() string
	License() string
	NodeNames() []string
	NodeName(typ int) string
	NodeInt(name string) int
	Parsers() map[string]NodeParser
	Checks() map[string]Check
	Classes() map[string]Class
	Tokens() map[string]Token
	PEGN() string // returns original grammar.pegn content
}

type GrammarComponent interface {
	Name() string
	Desc() string
	PEGN() string
}

type NodeParser interface {
	GrammarComponent
	Parse(p Parser) (*Node, error)
}

type Check interface {
	GrammarComponent
	Check(p Parser) (*Mark, error)
}

type Class interface {
	GrammarComponent
	Is(r rune) bool
}

type Token interface {
	GrammarComponent
	Value() string
}
