package parser

import (
	"io"

	"gitlab.com/pegn/pegn-go"
)

func New() pegn.Parser {
	p := new(parser)
	return p
}

type parser struct {
	in  io.Reader
	buf []byte
	mk  *pegn.Mark
}
